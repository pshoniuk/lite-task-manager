import { random } from 'lodash'

export default () => `https://picsum.photos/${random(300, 500)}/${random(100,200)}/`
