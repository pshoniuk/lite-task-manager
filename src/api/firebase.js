import * as firebase from 'firebase'

const config = {
  apiKey: 'AIzaSyBct4sT0i7_KP9YZ9t4HDIQgiPfFuwrPu8',
  authDomain: 'litetaskmanager.firebaseapp.com',
  databaseURL: 'https://litetaskmanager.firebaseio.com',
  projectId: 'litetaskmanager',
  storageBucket: 'litetaskmanager.appspot.com',
  messagingSenderId: '236588495442'
}

export default firebase.initializeApp(config)
