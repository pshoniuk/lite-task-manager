import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import Button from 'material-ui/Button'
import IconButton from 'material-ui/IconButton'
import Dialog, { DialogActions, DialogContent, DialogTitle } from 'material-ui/Dialog'
import Card, { CardMedia, CardHeader, CardActions } from 'material-ui/Card'
import TextField from 'material-ui/TextField'
import ClearIcon from 'material-ui-icons/Clear'
import Refresh from 'material-ui-icons/Refresh'
import getRandomImageUrl from '../../api/getRandomImageUrl'
import fileToDataURL from '../../utils/fileToDataURL'
import TaskShape from '../../shapes/Task'
import styles from './TaskDetailsModal.scss'

export const MAX_IMAGE_FILE_SIZE = 1024*750
export const MAX_IMAGE_FILE_SIZE_IN_KB = Math.round(MAX_IMAGE_FILE_SIZE/1024)

export default class TaskEditModal extends React.Component {
  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
    toggle: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired,
    task: TaskShape.isRequired,
    title: PropTypes.string,
    addButtonText: PropTypes.string,
    cancelButtonText: PropTypes.string,
  }

  static defaultProps = {
    addButtonText: 'Save',
    cancelButtonText: 'Close',
  }

  constructor(props) {
    super(props)
    this.imageInputRef = null
    this.state = {
      task: { ...this.props.task },
      dataErrors: {},
    }
  }

  changeData(changes = {}) {
    this.setState({ task: { ...this.state.task, ...changes } })
  }

  onNameChange = ({ target }) => {
    this.changeData({ name: target.value })
  }

  onDescriptionChange = ({ target }) => {
    this.changeData({ description: target.value })
  }

  onImageChange = ({ target }) => {
    const selectedImage = target.files[0]

    if (selectedImage) {
      if (selectedImage.size > MAX_IMAGE_FILE_SIZE) {
        this.setState({ dataErrors: {
          ...this.state.dataErrors,
          image: `Max image size is ${MAX_IMAGE_FILE_SIZE_IN_KB}Kb`,
        }})
      } else {
        // It's bad, but I ran out of coffee
        fileToDataURL(target.files).then((dataURL) => {
          this.changeData({ imageUrl: dataURL })
        })
      }
    }
  }

  setRandomImage = () => {
    this.changeData({ imageUrl: getRandomImageUrl() })
  }

  resetSelectedImage = () => {
    this.changeData({ imageUrl: null })

    if (this.imageInputRef) {
      this.imageInputRef.value = ''
    }
  }

  onTaskEdit = () => {
    this.validateData(() => {
      if (_.isEmpty(this.state.dataErrors)) {
        this.props.onEdit(this.state.task)
      }
    })
  }

  validateData(callback = null) {
    const dataErrors = _({
      name: [this.state.task.name, 'Name is required'],
    })
      .pickBy(v => !_.first(v))
      .mapValues(v => _.last(v))
      .value()

    this.setState({ dataErrors }, callback)
  }

  renderImageAction() {
    return (
      <div>
        <IconButton onClick={this.resetSelectedImage}>
          <ClearIcon />
        </IconButton>
        <IconButton onClick={this.setRandomImage}>
          <Refresh />
        </IconButton>
      </div>
    )
  }

  render() {
    const uid = _.uniqueId('task-edit-modal')

    return (
      <div>
        <Dialog
          open={this.props.isOpen}
          onClose={this.props.toggle}
          fullWidth={true}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">{this.props.title}</DialogTitle>
          <DialogContent>
            <TextField
              value={this.state.task.name}
              onChange={this.onNameChange}
              error={!!this.state.dataErrors.name}
              id={`${uid}_name`}
              label={this.state.dataErrors.name || 'Task Name'}
              margin="normal"
              required
              fullWidth
            />
            <TextField
              value={this.state.task.description}
              onChange={this.onDescriptionChange}
              id={`${uid}_description`}
              label="Task Description"
              rows="4"
              margin="normal"
              multiline
              fullWidth
            />
            <Card>
              <CardHeader
                subheader='This is task picture'
                action={this.renderImageAction()}
              />
              {this.state.task.imageUrl && (
                <CardMedia
                  className={styles.taskEditCardMedia}
                  image={this.state.task.imageUrl}
                  title={this.state.task.name}
                />
              )}
              <CardActions>
                <TextField
                  onChange={this.onImageChange}
                  error={!!this.state.dataErrors.image}
                  id={`${uid}_description`}
                  label={this.state.dataErrors.image || ''}
                  margin="normal"
                  type="file"
                  inputRef={(ref) => { this.imageInputRef = ref }}
                  inputProps={{accept: "image/*"}}
                  fullWidth
                />
              </CardActions>
            </Card>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.props.toggle} color="primary">
              {this.props.cancelButtonText}
            </Button>
            <Button onClick={this.onTaskEdit} color="primary">
              {this.props.addButtonText}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}
