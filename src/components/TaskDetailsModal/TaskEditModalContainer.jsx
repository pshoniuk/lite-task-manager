import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as tasksActions } from '../../redux/modules/tasks'
import { getDisplayName } from '../../utils/react'
import toJS from '../toJS'
import TaskShape from '../../shapes/Task'
import TaskDetailsModal from './TaskDetailsModal'

class TaskEditModalContainer extends React.Component {
  static displayName = `taskEditModalContainer${getDisplayName(TaskDetailsModal)}`

  static propTypes = {
    task: TaskShape.isRequired,
    isOpen: PropTypes.bool.isRequired,
    toggle: PropTypes.func.isRequired,
    updateTask: PropTypes.func,
  }

  updateTask = (data = {}) => {
    const stillKeys = _.keys(_.pickBy(data, (v, k) => v === this.props.task[k]))
    this.props.toggle()
    this.props.updateTask(data.key, _.omit(data, stillKeys))
  }

  render() {
    return (
      <TaskDetailsModal
        {...this.props}
        title="Edit Task"
        addButtonText="Save"
        cancelButtonText="Cancel"
        onEdit={this.updateTask}
      />
    )
  }
}

const mapStateToProps = () => {
  return {}
}

const mapDispatchToProps = dispatch => {
  return {
    updateTask: bindActionCreators(tasksActions.updateItem, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(toJS(TaskEditModalContainer))
