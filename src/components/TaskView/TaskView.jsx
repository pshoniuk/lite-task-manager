import _ from 'lodash'
import moment from 'moment'
import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import Grid from 'material-ui/Grid'
import Menu, { MenuItem } from 'material-ui/Menu'
import Card, { CardActions, CardHeader, CardContent, CardMedia } from 'material-ui/Card'
import Button from 'material-ui/Button'
import IconButton from 'material-ui/IconButton'
import Typography from 'material-ui/Typography'
import MoreVertIcon from 'material-ui-icons/MoreVert'
import FavoriteIcon from 'material-ui-icons/Favorite'
import TaskShape from '../../shapes/Task'
import { TaskEditModal } from '../TaskDetailsModal'
import FullContentDialog from './FullContentDialog'
import styles from './TaskView.scss'

export default class TaskView extends React.Component {
  static propTypes = {
    task: TaskShape.isRequired,
    isSelected: PropTypes.bool,
    update: PropTypes.func.isRequired,
    remove: PropTypes.func.isRequired,
    toggleSelected: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
    this.uid = _.uniqueId('task-view')
    this.state = {
      cardMenuAnchorEl: null,
      isOpenEditTaskDialog: false,
      isOpenFullConDialog: false,
    }
  }

  getTruncatedDescription() {
    return _.truncate(this.props.task.description, {
      'length': 170,
      'separator': /,? +/,
    })
  }

  openCardMenu = (e) => {
    e.stopPropagation()
    this.setState({ cardMenuAnchorEl: e.currentTarget })
  }

  closeCardMenu = () => {
    this.setState({ cardMenuAnchorEl: null })
  }

  toggleEditTaskDialog = () => {
    this.setState((state) => ({
      isOpenEditTaskDialog: !state.isOpenEditTaskDialog,
    }))
  }

  toggleFullContentDialog = () => {
    this.setState((state) => ({
      isOpenFullConDialog: !state.isOpenFullConDialog,
    }))
  }

  onSelfClick = () => {
    if (!this.state.cardMenuAnchorEl) {
      this.props.toggleSelected(this.props.task.key)
    }
  }

  onLearnMoreClick = (e) => {
    e.stopPropagation()
    this.toggleFullContentDialog()
  }

  onAddFavoriteClick = (e) => {
    e.stopPropagation()
    this.props.update(this.props.task.key, {
      favorite: !this.props.task.favorite,
    })
  }

  onEditClick = () => {
    this.toggleEditTaskDialog()
    this.closeCardMenu()
  }

  onRemoveClick = () => {
    this.closeCardMenu()
    this.props.remove(this.props.task.key)
  }

  renderCardHeaderAction() {
    const isOpenCardMenu = !!this.state.cardMenuAnchorEl
    return (
      <div>
        <IconButton
          aria-label="More"
          aria-owns={isOpenCardMenu ? `task-view-menu-${this.uid}` : null}
          aria-haspopup="true"
          onClick={this.openCardMenu}
        >
          <MoreVertIcon />
        </IconButton>
        <Menu
          id={`task-view-menu-${this.uid}`}
          anchorEl={this.state.cardMenuAnchorEl}
          open={isOpenCardMenu}
          onClose={this.closeCardMenu}
        >
          <MenuItem onClick={this.onEditClick}>Edit</MenuItem>
          <MenuItem onClick={this.onRemoveClick}>Remove</MenuItem>
        </Menu>
      </div>
    )
  }

  render() {
    const truncatedDescription = this.getTruncatedDescription()
    return (
      <Grid item xs={12} sm={6} md={4} lg={3}>
        <Card
          onClick={this.onSelfClick}
          className={classNames({ [styles.selected]: this.props.isSelected })}
        >
          <CardHeader
            title={this.props.task.name}
            subheader={moment(this.props.task.createdAt).format('MMMM D, YYYY h:mm')}
            action={this.renderCardHeaderAction()}
          />
          {this.props.task.imageUrl && (
            <CardMedia
              className={classNames(styles.taskViewCardMedia)}
              image={this.props.task.imageUrl}
              title={this.props.task.name}
            />
          )}
          <CardContent>
            <Typography component="p">
              {truncatedDescription}
            </Typography>
          </CardContent>
          <CardActions>
            <IconButton
              aria-label="Add to favorites"
              color={this.props.task.favorite ? "accent" : "default"}
              onClick={this.onAddFavoriteClick}
            >
              <FavoriteIcon />
            </IconButton>
            {this.props.task.description.length > truncatedDescription.length && (
              <Button dense color="primary" onClick={this.onLearnMoreClick}>
                LEARN MORE
              </Button>
            )}
          </CardActions>
        </Card>
        <TaskEditModal
          task={this.props.task}
          isOpen={this.state.isOpenEditTaskDialog}
          toggle={this.toggleEditTaskDialog}
        />
        <FullContentDialog
          isOpen={this.state.isOpenFullConDialog}
          toggle={this.toggleFullContentDialog}
          task={this.props.task}
        />
      </Grid>
    )
  }
}
