import React from 'react'
import PropTypes from 'prop-types'
import Grid from 'material-ui/Grid'
import TaskShape from '../../shapes/Task'
import TaskView from '../TaskView'
import tasksGridContainer from './tasksGridContainer'

class TasksGrid extends React.Component {
  static propTypes = {
    tasks: PropTypes.arrayOf(TaskShape).isRequired,
    selectedTasksKeys: PropTypes.arrayOf(PropTypes.string).isRequired,
    updateTask: PropTypes.func.isRequired,
    removeTask: PropTypes.func.isRequired,
    toggleTaskSelected: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {}
  }

  updateTask = (key, updates = {}) => {
    this.props.updateTask(key, updates)
  }

  removeTask = (key) => {
    this.props.removeTask(key)
  }

  toggleTaskSelected = (key) => {
    this.props.toggleTaskSelected(key)
  }

  renderTasks() {
    return this.props.tasks.map((task = {}) => (
      <TaskView
        key={task.key}
        task={task}
        isSelected={this.props.selectedTasksKeys.includes(task.key)}
        update={this.updateTask}
        remove={this.removeTask}
        toggleSelected={this.toggleTaskSelected}
      />
    ))
  }

  render() {
    return (
      <Grid container spacing={24} alignContent="space-between">
        {this.renderTasks()}
      </Grid>
    )
  }
}


export default tasksGridContainer(TasksGrid)
