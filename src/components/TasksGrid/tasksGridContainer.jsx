import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as tasksActions, selectors as tasksSelectors } from '../../redux/modules/tasks'
import { getDisplayName } from '../../utils/react'
import toJS from '../toJS'

export default WrappedComponent => {
  const mapStateToProps = state => {
    return {
      tasks: tasksSelectors.tasks(state),
      selectedTasksKeys: tasksSelectors.getSelectedTasksKeys(state),
    }
  }

  const mapDispatchToProps = dispatch => {
    return {
      updateTask: bindActionCreators(tasksActions.updateItem, dispatch),
      removeTask: bindActionCreators(tasksActions.remove, dispatch),
      toggleTaskSelected: bindActionCreators(tasksActions.toggleSelected, dispatch),
    }
  }

  function Component(props) {
    return <WrappedComponent {...props} />
  }

  Component.displayName = `tasksGridContainer${getDisplayName(WrappedComponent)}`

  return connect(mapStateToProps, mapDispatchToProps)(toJS(Component))
}
