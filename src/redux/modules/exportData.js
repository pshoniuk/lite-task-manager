import { fromJS } from "immutable"
import { createSelector } from "reselect"
import ExportDataModel from '../models/ExportData'

const getActionType = type => `export/${type}`
const initialState = new ExportDataModel()

export const actionTypes = {
  EXPORT_DATA_TO_FILE: getActionType('EXPORT_DATA_TO_FILE'),
  EXPORT_DATA_TO_FILE_SUCCESS: getActionType('EXPORT_DATA_TO_FILE_SUCCESS'),
  EXPORT_DATA_TO_FILE_ERROR: getActionType('EXPORT_DATA_TO_FILE_ERROR'),
}

export const actions = {
  exportDataToFile: (data) => {
    return {
      type: actionTypes.EXPORT_DATA_TO_FILE,
      payload: data,
    }
  },
  exportDataToFileSuccess: () => {
    return {
      type: actionTypes.EXPORT_DATA_TO_FILE_SUCCESS,
      payload: null,
    }
  },
  exportDataToFileError: (error) => {
    return {
      type: actionTypes.EXPORT_DATA_TO_FILE_ERROR,
      payload: error,
    }
  }
}

const getRoot = (state) => state.get('export')
const isInProgress = createSelector(getRoot, (state) => state.get('inProgress').some(v => !!v))

export const selectors = {
  root: getRoot,
  isInProgress: isInProgress,
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case actionTypes.EXPORT_DATA_TO_FILE:
      return state
        .setIn(['inProgress', actionTypes.EXPORT_DATA_TO_FILE], true)
        .setIn(['errors', actionTypes.EXPORT_DATA_TO_FILE], null)
    case actionTypes.EXPORT_DATA_TO_FILE_SUCCESS:
      return state
        .setIn(['inProgress', actionTypes.EXPORT_DATA_TO_FILE], false)
        .setIn(['errors', actionTypes.EXPORT_DATA_TO_FILE], null)
    case actionTypes.EXPORT_DATA_TO_FILE_ERROR:
      return state
        .setIn(['inProgress', actionTypes.EXPORT_DATA_TO_FILE], false)
        .setIn(['errors', actionTypes.EXPORT_DATA_TO_FILE], fromJS(action.payload))

    default:
      return state
  }
}
