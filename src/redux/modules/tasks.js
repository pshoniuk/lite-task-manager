import _ from 'lodash'
import { createSelector } from 'reselect'
import { fromJS, List, Set } from 'immutable'
import { AddTaskModel } from '../models'

const getActionType = type => `tasks/${type}`
const initialState = new AddTaskModel()

export const actionTypes = {
  UPDATE_LIST: getActionType('UPDATE_LIST'),
  CREATE: getActionType('CREATE'),
  CREATE_SUCCESS: getActionType('CREATE_SUCCESS'),
  CREATE_ERROR: getActionType('CREATE_ERROR'),
  UPDATE_ITEM: getActionType('UPDATE_ITEM'),
  UPDATE_ITEM_SUCCESS: getActionType('UPDATE_ITEM_SUCCESS'),
  UPDATE_ITEM_ERROR: getActionType('UPDATE_ITEM_ERROR'),
  REMOVE: getActionType('REMOVE'),
  REMOVE_SUCCESS: getActionType('REMOVE_SUCCESS'),
  REMOVE_ERROR: getActionType('REMOVE_ERROR'),
  SELECT: getActionType('SELECT'),
  DESELECT: getActionType('DESELECT'),
  TOGGLE_SELECTED: getActionType('TOGGLE_SELECTED'),
}

export const actions = {
  updateList: (tasks) => {
    return {
      type: actionTypes.UPDATE_LIST,
      payload: tasks,
    }
  },
  create: data => {
    return {
      type: actionTypes.CREATE,
      payload: data,
    }
  },
  createSuccess: () => {
    return {
      type: actionTypes.CREATE_SUCCESS,
      payload: null,
    }
  },
  createError: error => {
    return {
      type: actionTypes.CREATE_ERROR,
      payload: error,
    }
  },
  updateItem: (key, updates) => {
    return {
      type: actionTypes.UPDATE_ITEM,
      payload: { key, updates },
    }
  },
  updateItemSuccess: () => {
    return {
      type: actionTypes.UPDATE_ITEM_SUCCESS,
      payload: null,
    }
  },
  updateItemError: error => {
    return {
      type: actionTypes.UPDATE_ITEM_ERROR,
      payload: error,
    }
  },
  remove: key => {
    return {
      type: actionTypes.REMOVE,
      payload: key,
    }
  },
  removeSuccess: () => {
    return {
      type: actionTypes.REMOVE_SUCCESS,
      payload: null,
    }
  },
  removeError: error => {
    return {
      type: actionTypes.REMOVE_ERROR,
      payload: error,
    }
  },
  select: key => {
    return {
      type: actionTypes.SELECT,
      payload: key,
    }
  },
  deselect: key => {
    return {
      type: actionTypes.DESELECT,
      payload: key,
    }
  },
  toggleSelected: key => {
    return {
      type: actionTypes.TOGGLE_SELECTED,
      payload: key,
    }
  },
}

const getRoot = (state) => state.get('tasks')
const getTasks = createSelector(getRoot, (state) => state.get('data', List()))
const getSelectedTasksKeys = createSelector(getRoot, (state) => state.get('selected', Set()))
const getSelectedTasks = createSelector([
  getTasks,
  getSelectedTasksKeys
], (tasks, selectedKeys) => tasks.filter((task) => selectedKeys.has(task.get('key'))))
const isInProgress = createSelector(getRoot, (state) => state.get('inProgress').some(v => !!v))

export const selectors = {
  root: getRoot,
  tasks: getTasks,
  getSelectedTasksKeys: getSelectedTasksKeys,
  selectedTasks: getSelectedTasks,
  isInProgress: isInProgress,
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case actionTypes.UPDATE_LIST:
      return state.set('data', List(fromJS(action.payload)))

    case actionTypes.CREATE:
      return state
        .setIn(['inProgress', actionTypes.CREATE], true)
        .setIn(['errors', actionTypes.CREATE], null)
    case actionTypes.CREATE_SUCCESS:
      return state
        .setIn(['inProgress', actionTypes.CREATE], false)
        .setIn(['errors', actionTypes.CREATE], null)
    case actionTypes.CREATE_ERROR:
      return state
        .setIn(['inProgress', actionTypes.CREATE], false)
        .setIn(['errors', actionTypes.CREATE], fromJS(action.payload))

    case actionTypes.UPDATE_ITEM:
      return state
        .setIn(['inProgress', actionTypes.UPDATE_ITEM], true)
        .setIn(['errors', actionTypes.UPDATE_ITEM], null)
    case actionTypes.UPDATE_ITEM_SUCCESS:
      return state
        .setIn(['inProgress', actionTypes.UPDATE_ITEM], false)
        .setIn(['errors', actionTypes.UPDATE_ITEM], null)
    case actionTypes.UPDATE_ITEM_ERROR:
      return state
        .setIn(['inProgress', actionTypes.UPDATE_ITEM], false)
        .setIn(['errors', actionTypes.UPDATE_ITEM], fromJS(action.payload))

    case actionTypes.REMOVE:
      return state
        .setIn(['inProgress', actionTypes.REMOVE], true)
        .setIn(['errors', actionTypes.REMOVE], null)
        .update('selected', (selected = Set()) => {
          const keys = _.castArray(action.payload)
          return selected.filter((key) => !keys.includes(key))
        })
    case actionTypes.REMOVE_SUCCESS:
      return state
        .setIn(['inProgress', actionTypes.REMOVE], false)
        .setIn(['errors', actionTypes.REMOVE], null)
    case actionTypes.REMOVE_ERROR:
      return state
        .setIn(['inProgress', actionTypes.REMOVE], false)
        .setIn(['errors', actionTypes.REMOVE], fromJS(action.payload))

    case actionTypes.SELECT:
        return state.update('selected', (selected = Set()) => selected.add(action.payload))
    case actionTypes.DESELECT:
      return state.update('selected', (selected = Set()) => selected.remove(action.payload))
    case actionTypes.TOGGLE_SELECTED:
      return state.update('selected', (selected = Set()) => {
        if (selected.has(action.payload)) {
          return selected.remove(action.payload)
        }
        return selected.add(action.payload)
      })

    default:
      return state
  }
}
