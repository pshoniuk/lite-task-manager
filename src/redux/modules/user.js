import { createSelector } from 'reselect'
import { UserModel, UserProfileModel } from '../models'

const getActionType = type => `user/${type}`
const initialState = new UserModel()

export const actionTypes = {
  AUTH_PREPARE: getActionType('AUTH_PREPARE'),
  AUTH_PREPARE_SUCCESS: getActionType('AUTH_PREPARE_SUCCESS'),
  AUTH_PREPARE_ERROR: getActionType('AUTH_PREPARE_ERROR'),
  LOGIN: getActionType('LOGIN'),
  LOGIN_SUCCESS: getActionType('LOGIN_SUCCESS'),
  LOGIN_ERROR: getActionType('LOGIN_ERROR'),
  LOGOUT: getActionType('logout'),
}

export const actions = {
  authPrepare: () => {
    return {
      type: actionTypes.AUTH_PREPARE,
      payload: null,
    }
  },
  authPrepareSuccess: () => {
    return {
      type: actionTypes.AUTH_PREPARE_SUCCESS,
      payload: null,
    }
  },
  authPrepareError: () => {
    return {
      type: actionTypes.AUTH_PREPARE_ERROR,
      payload: null,
    }
  },
  login: provider => {
    return {
      type: actionTypes.LOGIN,
      payload: provider,
    }
  },
  loginSuccess: data => {
    return {
      type: actionTypes.LOGIN_SUCCESS,
      payload: data,
    }
  },
  loginError: error => {
    return {
      type: actionTypes.LOGIN_ERROR,
      payload: error,
    }
  },
  logout: () => {
    return {
      type: actionTypes.LOGOUT,
      payload: null,
    }
  },
}

const getRootState = state => state.get('user')
const isAuthPreparing = createSelector(getRootState, user => user.get('isAuthPreparing'))
const isLogin = createSelector(getRootState, user => user.get('isLogin'))
const getProfile = createSelector(getRootState, user => user.get('data'))

export const selectors = {
  root: getRootState,
  isAuthPreparing: isAuthPreparing,
  isLogin: isLogin,
  profile: getProfile,
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case actionTypes.AUTH_PREPARE:
      return new UserModel({
        data: new UserProfileModel(),
        isLogin: false,
        isAuthPreparing: true,
        loginError: null,
      })
    case actionTypes.AUTH_PREPARE_SUCCESS:
      return state.set('isAuthPreparing', false)
    case actionTypes.AUTH_PREPARE_ERROR:
      return new UserModel({
        data: new UserProfileModel(),
        isLogin: false,
        isAuthPreparing: false,
        loginError: action.payload,
      })
    case actionTypes.LOGIN:
      return new UserModel()
    case actionTypes.LOGIN_SUCCESS:
      return new UserModel({
        data: new UserProfileModel(action.payload),
        isLogin: true,
        isAuthPreparing: false,
        loginError: null,
      })
    case actionTypes.LOGIN_ERROR:
      return new UserModel({
        data: new UserProfileModel(),
        isLogin: false,
        authInProgress: false,
        loginError: action.payload,
      })
    case actionTypes.LOGOUT:
      return new UserModel()
    default:
      return state
  }
}
