import { all, fork } from 'redux-saga/effects'
import userSaga from './user'
import tasksSaga from './tasks'
import exportDataSaga from './exportData'

export default function* root() {
  yield all([fork(userSaga), fork(tasksSaga), fork(exportDataSaga)])
}
