import { Record } from 'immutable'
import UserProfileModel from './UserProfile'

export default Record({
  data: new UserProfileModel(),
  isAuthPreparing: false,
  isLogin: false,
  loginError: null,
})
