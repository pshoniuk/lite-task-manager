import { Record } from 'immutable'

export default Record({
  email: '',
  displayName: '',
  photoURL: '',
})
